deb_dist = ./deb-dist

.depsdeb:
	@echo python
	@echo python-setuptools
	@echo python-stdeb

.bashrc:
	@printf "# <PLI-MATLAB>\n"
	@printf "export MSEG_PLI_MATLAB_DIR=\"$(realpath $(dir $(lastword $(MAKEFILE_LIST))))\"\n"
	@printf "# </PLI-MATLAB>\n\n"

clean:
	@rm -r ./build 2> /dev/null || true
	@rm -r ./dist 2> /dev/null || true
	@rm -r ${deb_dist} 2> /dev/null || true
	@rm -r ./deb_dist 2> /dev/null || true
	@rm -r ./plimatlab.egg-info 2> /dev/null || true
	@# Deletes all installed files outside of the repository
	@xargs rm 2> /dev/null < .installed-files.log || true
	@rm .installed-files.log 2> /dev/null || true

install:
	@# installed files will be written into `.installed-files.log`
	python setup.py install --user --record .installed-files.log

package:
	@mkdir ${deb_dist} 2> /dev/null || true
	python setup.py --command-packages=stdeb.command bdist_deb
	@mv ./deb_dist/*.deb ${deb_dist}/
