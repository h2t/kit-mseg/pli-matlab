The MSeg Motion Segmentation Evaluation Framework
===

**MSeg** is a framework for the [ArmarX](https://armarx.humanoids.kit.edu/index.html) robot development environment,
built to run and evaluate motion segmentation algorithms written in C++, Java, Python or MATLAB. Please refer to the
[MSeg documentation](https://mseg.readthedocs.io/) for guides on how to install the framework and how to get started.

If you encounter a problem, feel free to open a new issue in the
[issue tracker](https://gitlab.com/h2t/kit-mseg/mseg/issues) or send an email to
<a href="mailto:incoming+h2t/kit-mseg/mseg@gitlab.com">incoming+h2t/kit-mseg/mseg@gitlab.com</a>
(this will create an issue in the issue tracker).

--- *The rest of this document is relevant for developers only* ---


# MATLAB Programming Language Interface for MSeg

This repository is the Programming Language Interface (PLI) for the MATLAB programming language. Algorithms written in
MATLAB can use these API's to communicate with the MSeg Core Module.

Ice does not support MATLAB. To make it possible to integrate algorithms written in MATLAB anyway, this repository
makes use of MATLAB's Python API. The MATLAB PLI is actually an adapter between MATLAB's Python engine and the MSeg
Python PLI.


# License

Licensed under the [GPL 2.0 License](./LICENSE.md).
