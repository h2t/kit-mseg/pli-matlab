"""
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::plis
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
"""

from mseg import Granularity

from DataExchangeProxy import DataExchangeProxy
import plipython

class SegmentationAlgorithm(plipython.Parameters):

    def __init__(self):

        plipython.Parameters.__init__(self)

        self.data = DataExchangeProxy()
        self.name = ""
        self.requiresTraining = False
        self.trainingGranularity = Granularity.Medium.value

    def getName(self):

        return self.name

    def getRequiresTraining(self):

        return self.requiresTraining

    def getTrainingGranularity(self):

        return self.trainingGranularity
