"""
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::plis
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
"""

import os, sys
from setuptools import setup, find_packages

installable_data_files = [
    ('/usr/share/mseg-matlab-pli/lib', [
        'lib/Granularity.m'
    ])
]

# If this is a user installation from source, omit installable_data_files
# and use MATLAB libs from source
if 'install' in sys.argv and '--user' in sys.argv and os.environ.get('MSEG_PLI_MATLAB_DIR') is not None:
    installable_data_files = []

setup(
    name='plimatlab',
    version='1.0.0',
    description='MSeg MATLAB PLI',
    author="Christian R. G. Dreher",
    author_email="christian.dreher@student.kit.edu",
    url="https://gitlab.com/h2t/kit-mseg/pli-matlab",
    license='GPL2',
    install_requires=['plipython'],
    keywords='motion segmentation ice interface',
    packages=find_packages(),
    include_package_data=True,
    data_files=installable_data_files
)
