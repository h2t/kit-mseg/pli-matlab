%%%
%
% This file is part of ArmarX.
%
% ArmarX is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License version 2 as
% published by the Free Software Foundation.
%
% ArmarX is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program. If not, see <http://www.gnu.org/licenses/>.
%
% @package    MSeg::plis
% @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
% @date       2017
% @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
%             GNU General Public License
%%%

classdef Granularity < uint32
    enumeration
        Fine (1)
        Medium (2)
        Rough (3)
    end
end
